Description:
------------

This is a wiki module. It implements a filter that provides basic structured
text formatting. Basically, the conventions (and much of the code) are taken
from phpwiki.sf.net.  See the filter tips page for all formatting rules.

Installation:
-------------

1. Activate this module as usual.

2. Activate the wiki filter by going to administer->input formats.  It's 
   very strongly suggested that a new input format be created (such as 
   "WikiText") because this module will escape all HTML (i.e. HTML will
   display as-is).
   Go into the configuration of the input format created or chosen and Enable
   the WikiText filter.

3. The filter's configuration has a setting for allowing users to enter
   any HTML for display.  By default this setting is off.

Support:
--------

Find latest version and report issues at http://drupal.org/project/wiki

Current Maintainer
----------
Moshe Weitzman <weitzman AT tejasa DOT com>

Authors
------

Olav Schettler (olav.schettler@contaire.de)
Matthew Schwartz (matt at mattschwartz dot net)
Moshe Weitzman <weitzman AT tejasa DOT com>

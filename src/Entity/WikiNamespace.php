<?php

namespace Drupal\wiki\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Page type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "wiki_namespace",
 *   label = @Translation("Namespace"),
 *   label_collection = @Translation("Namespaces"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\wiki\Form\WikiNamespaceForm",
 *       "edit" = "Drupal\wiki\Form\WikiNamespaceForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\wiki\ListBuilder\WikiNamespaceListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer wiki namespaces",
 *   bundle_of = "wiki_page",
 *   config_prefix = "wiki_namespace",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/wiki/namespace/add",
 *     "edit-form" = "/admin/structure/wiki/namespace/{wiki_namespace}",
 *     "delete-form" = "/admin/structure/wiki/namespace/{wiki_namespace}/delete",
 *     "collection" = "/admin/structure/wiki/namespace"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class WikiNamespace extends ConfigEntityBundleBase {

  /**
   * The machine name of this page type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the page type.
   *
   * @var string
   */
  protected $label;

}

<?php

namespace Drupal\wiki;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a page entity type.
 */
interface WikiPageInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the page title.
   *
   * @return string
   *   Title of the page.
   */
  public function getTitle();

  /**
   * Sets the page title.
   *
   * @param string $title
   *   The page title.
   *
   * @return \Drupal\wiki\WikiPageInterface
   *   The called page entity.
   */
  public function setTitle($title);

  /**
   * Gets the page creation timestamp.
   *
   * @return int
   *   Creation timestamp of the page.
   */
  public function getCreatedTime();

  /**
   * Sets the page creation timestamp.
   *
   * @param int $timestamp
   *   The page creation timestamp.
   *
   * @return \Drupal\wiki\WikiPageInterface
   *   The called page entity.
   */
  public function setCreatedTime($timestamp);

}

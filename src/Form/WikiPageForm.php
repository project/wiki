<?php

namespace Drupal\wiki\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the page entity edit forms.
 */
class WikiPageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New page %label has been created.', $message_arguments));
      $this->logger('wiki')->notice('Created new page %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The page %label has been updated.', $message_arguments));
      $this->logger('wiki')->notice('Updated new page %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.wiki_page.canonical', ['wiki_page' => $entity->id()]);
  }

}
